from sqlalchemy import Table
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

engine = sqlalchemy.create_engine('mysql+mysqlconnector://wp2022:gibsmir0815@127.0.0.1:3306/wp_2022')




class list_pos(Base):
    __table__ = Table('list_pos', Base.metadata, autoload=True, autoload_with=engine)

class list_lemma(Base):
    __table__ = Table('list_lemma', Base.metadata, autoload=True, autoload_with=engine)


class list_token(Base):
    __table__ = Table('list_token', Base.metadata, autoload=True, autoload_with=engine)


class sentences(Base):
    __table__ = Table('sentences', Base.metadata, autoload=True, autoload_with=engine)


class tokens(Base):
    __table__ = Table('tokens', Base.metadata, autoload=True, autoload_with=engine)


class articles(Base):
    __table__ = Table('articles', Base.metadata, autoload=True, autoload_with=engine)


if __name__ == "__main__":
    # Definition which database to use:
    #engine = create_engine('"mariadb+mariadbconnector://wp2022:gibsmir0815@127.0.0.1:3306/wp_2022"')
    
    # We need a session to execute queries:
    Session = sqlalchemy.orm.sessionmaker()
    Session.configure(bind=engine)
    Session = Session()

    pos = Session.query(list_pos).all()

    for p in pos:
        print(p.pos)

    art = Session.query(articles).first()
    print(art.title)
