import lateco.Items as items

def write_file(filename, d):
    outfile = open("/data/wp-2022/output-b/database/" + filename, "w")
    for p in d:
        outfile.write(str(d[p]) + "\t" + p + "\n")
    outfile.close()

def createDatabaseFiles():
    c = items.Corpus("/data/wp-2022/annotated/AA/")

    subcorpora = c.getSubcorpora()

    pk_list_tokens = 0
    tokens = {}

    pk_list_lemma = 0
    lemmata = {}
    
    pk_list_pos = 0
    pos = {}

    pk_articles = 0
    file_articles = open("/data/wp-2022/output-b/database/articles.csv", "w")

    pk_sentences = 0
    file_sentences = open("/data/wp-2022/output-b/database/sentences.csv", "w")

    pk_tokens = 0
    file_tokens = open("/data/wp-2022/output-b/database/tokens.csv", "w")

    for subcorpus in subcorpora:
        print(subcorpus)
        su = items.Subcorpus(subcorpus)
        for article in su.articles:        
            pk_articles += 1
            file_articles.write(str(pk_articles) + "\t" + article.title + "\t" + str(article.id) + "\t" + article.url + "\n")

            for sentence in article.sentences:
                pk_sentences += 1
                file_sentences.write(str(pk_sentences) + "\t" + str(pk_articles) + "\n")
                for token in sentence.tokens:
                    token.token = token.token.replace("\t", "")
                    token.lemma = token.lemma.replace("\t", "")

                    token.token = token.token.replace('\\', "")
                    token.lemma = token.lemma.replace("\\", "")

                    pk_tokens += 1
                    if not token.token in tokens:
                        pk_list_tokens += 1
                        tokens[token.token] = pk_list_tokens

                    if not token.lemma in lemmata:
                        pk_list_lemma += 1
                        lemmata[token.lemma] = pk_list_lemma

                    if not token.pos in pos:
                        pk_list_pos += 1
                        pos[token.pos] = pk_list_pos

                    file_tokens.write(str(pk_tokens) + "\t" + str(pk_sentences) + "\t" + str(tokens[token.token])  + "\t" + str(lemmata[token.lemma]) + "\t" + str(pos[token.pos]) + "\t" + str(token.isAlpha) + "\t" + str(token.isStop) + "\n")
    file_articles.close()
    file_sentences.close()
    file_tokens.close()

    write_file("list_pos.csv", pos)
    write_file("list_tokens.csv", tokens)
    write_file("list_lemmata.csv", lemmata)

if __name__ == "__main__":
    createDatabaseFiles()
    
    



                    
            