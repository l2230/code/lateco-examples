--- Extract certain token:
select tokens.pk_tokens , list_token.token, list_lemma.lemma,  list_pos.pos from tokens 
join list_token on tokens.fk_token = list_token.pk_list_token 
join list_lemma on tokens.fk_lemma = list_lemma.pk_list_lemma 
join list_pos on tokens.fk_pos = list_pos.pk_list_pos 

where list_lemma.lemma = "schlafen"

limit 10

--- Extract one sentence:

select tokens.pk_tokens , list_token.token, list_lemma.lemma,  list_pos.pos from tokens
join list_token on tokens.fk_token = list_token.pk_list_token
join list_lemma on tokens.fk_lemma = list_lemma.pk_list_lemma
join list_pos on tokens.fk_pos = list_pos.pk_list_pos

where tokens.fk_sentence  = 100

--- Extract one article:

select tokens.pk_tokens , list_token.token, list_lemma.lemma,  list_pos.pos from tokens
join list_token on tokens.fk_token = list_token.pk_list_token
join list_lemma on tokens.fk_lemma = list_lemma.pk_list_lemma
join list_pos on tokens.fk_pos = list_pos.pk_list_pos
join sentences  on sentences.pk_sentences = tokens.fk_sentence

where sentences.fk_article  = 1000


--- Lahm aber klappt wohl

select  tokens.pk_tokens, list_token.token, list_lemma.lemma,  list_pos.pos, list_token.pk_list_token, sentences.fk_article, tokens.fk_sentence from tokens
 join list_token on tokens.fk_token = list_token.pk_list_token
 join list_lemma on tokens.fk_lemma = list_lemma.pk_list_lemma
 join list_pos on tokens.fk_pos = list_pos.pk_list_pos
 join sentences on tokens.fk_sentence  = sentences.pk_sentences
 join articles  on sentences.fk_article = articles.pk_articles

where articles.pk_articles = 200 order by tokens.pk_tokens ;

--- Klappt nach Indizierung

select  tokens.pk_tokens, list_token.token, list_lemma.lemma,  list_pos.pos, list_token.pk_list_token, sentences.fk_article, tokens.fk_sentence from tokens
 join list_token on tokens.fk_token = list_token.pk_list_token
 join list_lemma on tokens.fk_lemma = list_lemma.pk_list_lemma
 join list_pos on tokens.fk_pos = list_pos.pk_list_pos
 join sentences on tokens.fk_sentence  = sentences.pk_sentences
 join articles  on sentences.fk_article = articles.pk_articles

where articles.title="Pfadfinder" order by tokens.pk_tokens ;

