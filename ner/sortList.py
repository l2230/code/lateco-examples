infile = open("/data/wp-2022/listen/ne_misc.csv")
lines = infile.readlines()
infile.close()

records = {}

for line in lines:
    line = line.strip()
    rec = line.split("\t")
    records[rec[0]] = int(rec[1])

outfile = open("/data/wp-2022/listen/ne_sorted_misc.csv", "w")
for w in sorted(records, key=records.get, reverse=True):
    outfile.write(w + "\t" +  str(records[w]) + "\n")
outfile.close()

