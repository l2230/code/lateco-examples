import lateco.Items as items
import collections
import sys
sys.path.insert(0,'..')

import settings


if __name__ == "__main__" :

    # The ner corpus:
    nerCorpus = items.Ner("/data/wp-2022/ner")

    # Getting the corpus
    print(settings.config["corpuspath"])
    c = items.Corpus(settings.config["corpuspath"])
    c.setName(sys.argv[1])

    folders = c.getFolderList()    
    print("Found folders: ", len(folders))

    # Iterating over the subcorpora
    counter = 1

    pers = {}
    orgs = {}
    locs = {}
    miscs = {}

    for f in folders:
        for subcorpus in c.getSubcorporaOfFolder(f):
            sc = items.Subcorpus(subcorpus)
            relSc = subcorpus.replace(settings.config["corpuspath"], "")

            for article in sc.articles:
                artPos = relSc[1:] + "/" + str(article.id)
                print(artPos)
                ners = nerCorpus.getNERbyArticle(artPos)

                for p in ners["PER"]:
                    if p in pers:
                        pers[p] = pers[p] + ners["PER"][p]
                    else:
                        pers[p] = ners["PER"][p]

                for l in ners["LOC"]:
                    if l in locs:
                        locs[l] = locs[l] + ners["LOC"][l]
                    else:
                        locs[l] = ners["LOC"][l]

                for o in ners["ORG"]:
                    if o in orgs:
                        orgs[o] = orgs[o] + ners["ORG"][o]
                    else:
                        orgs[o] = ners["ORG"][o]

                for m in ners["MISC"]:
                    if m in miscs:
                        miscs[m] = miscs[m] + ners["MISC"][m]
                    else:
                        miscs[m] = ners["MISC"][m]


                for ne in ners:
                    print(ne, ners[ne])

                print("----------------------")



    outfile = open("/data/wp-2022/listen/ne_per.csv", "w")
    for p in pers:
        outfile.write(p + "\t" + str(pers[p]) + "\n")
    outfile.close()

    outfile = open("/data/wp-2022/listen/ne_loc.csv", "w")
    for l in locs:
        outfile.write(l + "\t" + str(locs[l]) + "\n")
    outfile.close()

    outfile = open("/data/wp-2022/listen/ne_org.csv", "w")
    for o in orgs:
        outfile.write(o + "\t" + str(orgs[o]) + "\n")
    outfile.close()

    outfile = open("/data/wp-2022/listen/ne_misc.csv", "w")
    for m in miscs:
        outfile.write(m + "\t" + str(miscs[m]) + "\n")
    outfile.close()


            #ners = nerCorpus.getNERbyArticle("AV/wiki_24/1997297")

            #for ne in ners:
            #    print(ne, ners[ne])


