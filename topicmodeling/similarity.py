import lateco.Items as items
from gensim import corpora
from gensim import models
from gensim import similarities
import json

def loadArticles():
    infile = open("/data/wp-2022/listen/articles.csv")
    lines = infile.readlines()
    infile.close()

    articles = {}
    for line in lines:
        rec = line.strip().split("\t")
        if len(rec) == 2:
            articles[rec[0]] = rec[1]
    return articles



def readCorpus(corpus, infile):
    inputfile = open(infile, "r")
    inputlines = inputfile.readlines()
    inputfile.close()

    texts =  []
    titles = []
    categories = {}
    counter = 0
    for line in inputlines:
        rec = line.strip().split("\t")
        print(counter, " Article: ", rec[2], " Title: ", rec[1], " Category: ", rec[0])
        if rec[0] in categories:
            categories[rec[0]].append(counter)
        else:
            categories[rec[0]] = [counter]
        titles.append(rec[1])        
        article = corpus.getArticleByPosition(rec[2])
        text = []
        for s in article.sentences:
            for t in s.tokens:
                if t.isStop == "0":
                    text.append(t.lemma)
        counter = counter + 1
        texts.append(text)   
    outfile = open("categories.dat", "w")
    outfile.write(json.dumps(categories))

    outfile.close()

    return texts, titles, categories

def createIndex(texts):
    dictionary = corpora.Dictionary(texts)
    print("We got now a dictionary of size: ", len(dictionary))

    vecs = []
    for t in texts:
        vec = dictionary.doc2bow(t)
        vecs.append(vec)

    print("Vectors are calculated: ", len(vecs))
    tfidf = models.TfidfModel(vecs)
    print("Model done!")
    index = similarities.SparseMatrixSimilarity(tfidf[vecs], num_features=len(dictionary))

    index.save("index.dat")
    tfidf.save("tfidf.dat")
    dictionary.save("dictionary.dat")

    return dictionary, tfidf, index


def calculateSim(theArticle, dictionary, tfidf, index, categories):
    text = []

    for s in theArticle.sentences:
        for t in s.tokens:
            if t.isStop == "0":
                text.append(t.lemma)

    print("Now calculating similarity: ")
    query_bow = dictionary.doc2bow(text)
    sims = index[tfidf[query_bow]]
    catSums = {}
    
    for cat in categories:
        catSums[cat] = 0

    for document_number, score in sorted(enumerate(sims), key=lambda x: x[1], reverse=True):
        for cat in categories:
            if document_number in categories[cat]:
                catSums[cat] = catSums[cat] + score
        #print(str(document_number) + "\t" +  str(score) + "\t" +  titles[document_number])

    maxCat = max(catSums, key = catSums.get)
    print("The scores for the categories: ", catSums)    
    print("************ The person seems to be a: ", maxCat, " ************ ") 
    

if __name__ == "__main__":
    corpus = items.Corpus("/data/wp-2022/annotated/")
    allArticles = loadArticles()

    print("Found " + str(len(allArticles)) + " Articles!")

    texts, titles, categories = readCorpus(corpus, "input.csv")
    dictionary, tfidf, index = createIndex(texts)

    toCompare = " "

    print("************ Lets go: ************ ")

    while toCompare != "":
        toCompare = input("Enter a person's name (firstname lastname):")  
        print("Searching for: ", toCompare)

        if toCompare in allArticles:
            print("Found article: ", allArticles[toCompare])
            theArticle = corpus.getArticleByPosition( allArticles[toCompare])
            calculateSim(theArticle, dictionary, tfidf, index, categories)
        else:
            if toCompare == "":
                print("Bye, bye!")
            else:
                print("Sorry, cant find " + toCompare)
        print("--------------------------------------------------")






