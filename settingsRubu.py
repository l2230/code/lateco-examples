config = {}

##################################
# Some paths to your local corpus:
##################################

# corpuspath points to a whole corpus with subfolders:
config["corpuspath"] = "/storage/wp/wp-2022/annotated"
#config["corpuspath"] = "/data/corpus"


# folderpath points to one subfolder in a corpus. 
# Can be a subfolder of corpus above:
config["folderpath"] = "/storage/wp/wp-2022/annotated/AA/"

# subcorpus: one file of a corpus in lateco format:
config["subcorpus"] = "/storage/wp/wp-2022/annotated/AA/wiki_00"

#################################
# The outputfolder. All example 
# scripts are storing their output here:
################################

config["outputfolder"] = "/storage/wp-output/"

#################################
# ElasticSearch configuration:
#################################


# The URL to your local ElasticSearch instance:
config["elasServer"] = "http://wp-elasticsearch.fritz.box"

# The port of your local ElasticSearch instance:
config["elasPort"] = "9200"

# Username (leave empty if no authentication is activated):
config["elasUser"] = ""

# password  (leave empty if no authentication is activated):
config["elasPwd"] = ""

# index to search through
config["elasIndex"] = "wp-2022"

