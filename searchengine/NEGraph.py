import string
import sys
import lateco.Searchengine as SE
import lateco.Items as items
import random
import string

sys.path.append('/data/lateco/code/web-server')
import config

sys.path.insert(0,'..')
import settings 
import urllib3


def getNEs(position):
    position = position.replace("wp-2022/", "")
    nerCorpus = items.Ner("/data/wp-2022/ner")
    ners = nerCorpus.getNERbyArticle(position)
    print("Found NERS: ", len(ners["PER"]))

    return ners


def getArticlePosition(article):

    se = SE.Queries(config.settings["es-url"], config.settings["es-port"], config.settings["es-user"], config.settings["es-pwd"])
    seresults = se.matchPhrase("wp-2022-articles","article" , article, 0,  20, True)
        
    position = ""
    if len(seresults["hits"]["hits"]) == 0:
        print("Cant find the article ", article)        
    else:
        for result in seresults["hits"]["hits"]:
            #position = result["_source"]["position"].split("/")
            if result["_source"]["article"] == article:
                position = result["_source"]["position"]
                break
    return position



if __name__ == "__main__":
    urllib3.disable_warnings()
    startArticle = sys.argv[1]

    print("Start Article: ", startArticle)

    position = getArticlePosition(startArticle)

    print("Position: ", position)

    

    ners = getNEs(position)

    nodes = {}
    nodes[startArticle] = ''.join(random.choice(string.ascii_lowercase) for i in range(16))

    dotfile = open(startArticle.replace(" ", "") + ".dot", "w")
    dotfile.write("digraph G {\n")
    dotfile.write(nodes[startArticle] + " [label =\"" + startArticle + "\", color=\"blue\"]\n" )

    for ne in ners["PER"]:
        ne = ne.strip(".")
        ne = ne.replace("\"", "")
        ne = ne.replace("'", "")
        secondPos = getArticlePosition(ne)
        print("NE: ", ne , secondPos)
        if (not ne in nodes) and (secondPos.startswith("wp") and (" " in ne)):
            print("DOT LINE")
            nodeId = ''.join(random.choice(string.ascii_lowercase) for i in range(16))
            nodes[ne] = nodeId
            dotfile.write(nodes[ne] + " [label =\"" + ne + "\", color=\"blue\"]\n" )
            dotfile.write(nodes[startArticle] + " -> " + nodes[ne] + "\n")

            secondNers = getNEs(secondPos)            

            for ne2 in secondNers["PER"]:
                ne2 = ne2.strip(".")
                ne2 = ne2.replace("\"", "")
                ne2 = ne2.replace("'", "")
                if (" " in ne2) and (getArticlePosition(ne2).startswith("wp")):
                    if ne2 in nodes:
                        dotfile.write(nodes[ne] + " -> " + nodes[ne2] + "\n")
                    else:
                        nodeId2 = ''.join(random.choice(string.ascii_lowercase) for i in range(16))
                        nodes[ne2] = nodeId2
                        dotfile.write(nodes[ne2] + " [label =\"" + ne2 + "\", color=\"blue\"]\n" )
                        dotfile.write(nodes[ne] + " -> " + nodes[ne2] + "\n")

        print("----------------------")

    dotfile.write("}\n")
    dotfile.close()        
