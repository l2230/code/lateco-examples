#!/usr/bin/python
import sys
import lateco.Searchengine as SE
import lateco.Items as items

sys.path.insert(0,'..')
import settings 

#################################################
# Adopt these settings to your local properties!
# Connection and other details to ElasticSearch:
elasServer = settings.config["elasServer"]
elasPort = settings.config["elasPort"]
elasUser = settings.config["elasUser"]
elasPwd = settings.config["elasPwd"]
elasIndex = settings.config["elasIndex"]

# Which annotation should be searched through:
annotation = "lemma"
#################################################

if __name__ == "__main__":
    
    se = SE.Queries(elasServer, elasPort, elasUser, elasPwd)
    seresults = se.matchPhraseScroll(elasIndex, annotation, sys.argv[1], 1)

    position = items.Position()
    position.corpus = elasIndex
    
    for result in seresults:       
        ResultObject = SE.Result(result)        
        ResultObject.sentence.display()
        
        position.folder = result["_source"]["subcorpus"].split("/")[0]
        position.subcorpus = result["_source"]["subcorpus"].split("/")[1]
        position.article =  result["_source"]["article"]
        position.sentence = result["_id"].split("_")[1]
    
        print("Position in the corpus: ", position.getSentence())

        print("-----------------")

    print("Number of results: ", len(seresults))
    print("Done.")
