import lateco.Items as items
import sys
sys.path.insert(0,'..')

import settings 

# Lemma frequency list
lemmata = {}

# List of pos tags which are taken into account:
posList = ["NN", "NE"]

# For example, all the verbs:
#posList = ["VMFIN", "VAFIN", "VVFIN", "VAIMP", "VVIMP", "VVINF", "VAINF", "VMINF", "VVIZU", "VVPP", "VMPP", "VAPP"]


# Searching for the given lemma in the current subcorpus, print the found sentences to the outfile and store the lemma frequencies:
def search(subcorpus, outfileSentences):
    su = items.Subcorpus(subcorpus)

    for a in su.articles:
        for s in a.sentences:            
            for i in range(0, len(s.tokens)-1):
                if s.tokens[i].lemma == lemma:
                    outfileSentences.write(s.toString("token"))
                    outfileSentences.write("\n")
                    for j in range(0, len(s.tokens)-1):
                        if s.tokens[j].pos in posList:
                            if s.tokens[j].lemma in lemmata:
                                lemmata[s.tokens[j].lemma] = lemmata[s.tokens[j].lemma] + 1
                            else:
                                lemmata[s.tokens[j].lemma] = 1


############################

if __name__ == "__main__" :   
    lemma = sys.argv[1]

    c = items.Corpus(settings.config["corpuspath"])


    outfilename = settings.config["outputfolder"].rstrip("/") + "/Concordance_sentences_" + lemma + ".con"
    print(outfilename)
    outfileSentences = open(outfilename, "w")
    
    lemmaFreqs = open(settings.config["outputfolder"].rstrip("/") + "/Concordance_lemma_" + lemma + ".con", "w")
    wordCloud =  open(settings.config["outputfolder"].rstrip("/") + "/Concordance_wordcloud_" + lemma + ".con", "w")

    subcorpora = c.getSubcorpora()

    for subcorpus in subcorpora:
        print("Dealing with: ", subcorpus)
        search(subcorpus, outfileSentences)
    outfileSentences.close()

    ############# Writing the lemma frequencies:
    for lemma in lemmata:
        lemmaFreqs.write(lemma + "\t" + str(lemmata[lemma]) + "\n")

    lemmaFreqs.close()

    ############# The word cloud:
    lemmata = dict(sorted(lemmata.items(), key=lambda item: item[1]))

    lastHundred = list(lemmata.items())[-100:]

    for key in lastHundred:
        for i in range(0, int(key[1])):
            wordCloud.write(key[0] + "\n")
    wordCloud.close()
