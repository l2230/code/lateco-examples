import lateco.Items as items
import sys
sys.path.insert(0,'..')
import settings


    
################## At first, create a corpus from the path given as parameter to this script:
corpus = items.Corpus(settings.config["corpuspath"])
corpus.setName("MyFirstCorpus") # the name is important for the positions

################## Inside the corpus, the next level are the folders:
print("---------------------------------------------------------------")
folders = corpus.getFolderList()
print("A Folder in the corpus: ", folders[0])

################## Lets take a look at the subcorpora in the folder:
print("---------------------------------------------------------------")
subcorpora = corpus.getSubcorporaOfFolder(folders[0])

print("Found " + str(len(subcorpora)) + " subcorpora in the folder. The first 5 are:")

for x in range(0,5):
    print(subcorpora[x])
       
################## Lets create a subcorpus out the first file in the folder:
subcorpus = items.Subcorpus(subcorpora[0])

################## A closer look at the articles in the subcorpus:
print("---------------------------------------------------------------")
print("\nFound " + str(len(subcorpus.articles)) + " articles in " + subcorpus.filename + ". Titles of the first 5 articles:")

for x in range(0,5):
    print(subcorpus.articles[x].title)    
    
################## Lets get one article out of the subcorpus:
print("---------------------------------------------------------------")
article = subcorpus.articles[0]
print("Title of the extracted article: ", article.title)

################## Whats about the sentences in the article:
print("---------------------------------------------------------------")
print("There are " + str(len(article.sentences)) + " sentences in the artilcle. Lets display the 2th sentence:" )
print(article.sentences[1].display())
    
################## Last not least, a token:
print("---------------------------------------------------------------")
token = article.sentences[1].tokens[1]
print("The second token of the sentence above:")
print(token.token)
print(token.lemma)
print(token.pos)
print(token.isAlpha)
print(token.isStop)


################## Lets calculate the position of the token in the corpus:
print("---------------------------------------------------------------")
position = corpus.getName() + "/"
position = position + folders[0] + "/"
position = position + subcorpus.getName() + "/"
position = position + str(article.id) + "/"
position = position + "2/"

print("The position of the token, relative to the corpus: " , position)
