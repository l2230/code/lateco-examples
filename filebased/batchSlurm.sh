#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=partone
#SBATCH --tasks-per-node=1
#SBATCH --array=0-66

echo "HOSTNAME" $HOSTNAME
echo ${SLURM_ARRAY_TASK_ID}
#python3 /storage/lateco/code/helpers/applySpacyNEArray.py /storage/wp/wp-2022 ${SLURM_ARRAY_TASK_ID}
python3 /storage/lateco/code/lateco-examples/filebased/ngramModel.py /storage/wp/wp-2022/annotated/ ${SLURM_ARRAY_TASK_ID}
