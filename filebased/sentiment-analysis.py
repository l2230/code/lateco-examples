import lateco.Items as items
import lateco.SentiWs as sentiWs
import sys
sys.path.insert(0,'..')
import settings 


def analyse(subcorpus, sentiment, outputfile):
    su = items.Subcorpus(subcorpus)
    for a in su.articles:
        artValue = 0
        negNumber = 0
        posNumber = 0
        tokNumber = 0
        for s in a.sentences:            
            tokNumber = tokNumber + len(s.tokens)
            for token in s.tokens:
                if token.lemma in sentiment.positiv:
                    artValue = artValue + sentiment.positiv[token.lemma].value
                    posNumber = posNumber + 1
                if token.lemma in sentiment.negativ:
                    artValue = artValue + sentiment.negativ[token.lemma].value        
                    negNumber = negNumber + 1
        outputFile.write("\"" +a.title + "\"\t" + str(artValue)+ "\t" + str(posNumber) + "\t" + str(negNumber) + "\t" + str(tokNumber) + "\n")
                
                

                



if __name__ == "__main__":
    sentimentData = settings.config["sentiment-data"]
    sentiment = sentiWs.Sentiment(sentimentData + "SentiWS_v1.8c_Positive.txt", sentimentData + "SentiWS_v1.8c_Negative.txt")

    print("Size positive:", len(sentiment.positiv))
    print("Size negativ:", len(sentiment.negativ))

    outputFile = open(settings.config["outputfolder"] + "sentiment.csv", "w")

    c = items.Corpus(settings.config["corpuspath"])
    subcorpora = c.getSubcorpora()

    for subcorpus in subcorpora:
        print("Dealing with: ", subcorpus)
        analyse(subcorpus, sentiment, outputFile)
    
    outputFile.close()
    print("Done.")