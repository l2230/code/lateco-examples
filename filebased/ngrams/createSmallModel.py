import glob

bigModelPath = "/data/wp-2022/snpOutput/ngrams/bigModel/"
smallModelPath = "/data/wp-2022/snpOutput/ngrams/smallModel/"

fileNames = glob.glob(bigModelPath + "*csv")



for f in fileNames:
    print("Dealing with ", f)
    inputFile = open(f, "r")
    outputFile = open(f.replace(bigModelPath, smallModelPath), "w")


    lines = inputFile.readlines()

    for line in lines:
        rec = line.strip().split("\t")
        number = int(rec[1])
        if number > 2:
            outputFile.write(line)

    inputFile.close()
    outputFile.close()