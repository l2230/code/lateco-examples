# Provenance ngrams

## extractTrigrams.py

Iterates over all files in an annotated corpus and create a file with all trigrams:

```
/data/wp-2022/snpOutput/trigrams.json
```


##  countThroughTrigrams.py

Goes through the trigram file from above and creates individual files for trigrams starting with 'A', 'B' etc. The _batch.sh_ collects calls to this script. 

__Currently filenames are hard coded in countThroughTrigrams.py__

## writeText.py

Uses the files from above to create new sentences. Takes a bigram and the maximum number of words to add as parameter.

```
python3 writeText.py "Die Butter" 20

```