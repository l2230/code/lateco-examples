import lateco.Items as items
import json
import sys
sys.path.insert(0,'..')

import settings 

import os



def folderwalk(walk_dir, ending):

        liste = []
        for root, dirs, files in os.walk(walk_dir):
                path = root.split('/')
                for file in files:
                        if file.endswith(ending):
                                liste.append(os.path.join(root, file))

        return liste


if __name__ == "__main__" :   
    #basedir = sys.argv[1]
    #print("Basedir: ", basedir)
    inputfolders = ["AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ","BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ","CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO"]

    #theFolder = basedir + inputfolders[int(sys.argv[2])]
    #print("Folder: ", theFolder)

    theFolder = "/data/wp-2022/annotated/"

    outputfile = open("/data/wp-2022/snpOutput/trigrams.json", "w")

    trigrams = {}

    files = folderwalk(theFolder, "")

    for subcorpus in files:
        print("Subcorpus: ", subcorpus)
        # "/data/wp-2022/annotated/AA/wiki_00"
        su = items.Subcorpus(subcorpus)

        for a in su.articles:
            for s in a.sentences:
                #print(s.toString("token"))
                counter = 0
                while counter < len(s.tokens)-2:
                    w1 = s.tokens[counter].token
                    w2 = s.tokens[counter + 1].token
                    w3 = s.tokens[counter + 2].token
                    counter = counter + 1
                    outputfile.write(w1 + " " + w2 + " " + w3 + "\n")

    outputfile.close()            

