import sys
import operator
import os
import random
import glob

startText = sys.argv[1]
numberIterations = int(sys.argv[2])

#ngramPath = "/data/wp-2022/snpOutput/ngrams/"

ngramPath = sys.argv[3]

print("Start text: ", startText)

rec = startText.split(" ")

iteration = 0

while iteration < numberIterations:
    iteration = iteration + 1
    print("Iteration: ", iteration)
    firstWord = rec[-2]
    secondWord = rec[-1]

    print("First words: ", firstWord, secondWord)

    if firstWord == ".":
        fileToOpen = ngramPath + "point.csv"
        #iteration = 10
    elif firstWord in '/(),;:!?':
        fileToOpen = ngramPath + "specialCharacters.csv"
    elif firstWord in '"“':
        fileToOpen = ngramPath + "quotes.csv"
    else:
        fileToOpen =  ngramPath + firstWord[0].upper() + ".csv"
    
    print("File to open: ", fileToOpen)

    
    if os.path.isfile(fileToOpen):
        ngramFile = open(fileToOpen, "r")
    else:
        print("Sorry can't find the file: ", fileToOpen)
        break

    trigrams = {}

    for line in ngramFile:
        trigram = line.strip().split("\t")[0].split(" ")
        theNumber = int(line.strip().split("\t")[1])


        if trigram[0] == firstWord and trigram[1] == secondWord:            
            trigrams[trigram[2]] = theNumber

    if len(trigrams) == 0:
        print("Sorry, couldn't find a bigram: ", firstWord, secondWord)
        break
    else:
        print("Found trigrams: ", len(trigrams))

    sorted_trigrams = sorted(trigrams.items(), key=lambda x:x[1], reverse=True)
    if len(sorted_trigrams) > 10:
        sorted_trigrams = sorted_trigrams[0:10]
    #print(sorted_trigrams)

    nextWord = "("

    while nextWord in "()\";:„":
        nextWord= random.choice(sorted_trigrams)[0]
    print("Choosen: ", nextWord)


    #nextWord = max(trigrams.items(), key=operator.itemgetter(1))[0]

    rec.append(nextWord)

    print(rec)
    print("------------------------------------------------")

    if nextWord == ".":
        break


    ngramFile.close()

print("************************")    

print("The final text: ")
print(" ".join(rec))
print("Done.")


