import sys

startChar = sys.argv[1]

print("Start Char: ", startChar)

basePath = "/data/wp-2022/snpOutput/ngrams/"
trigramfile = open(basePath + "trigrams.json", "r")

d = {}
counter = 0

for line in trigramfile:
    line = line.strip()
    counter = counter + 1
    if counter % 100000 == 0:
        print(counter, len(d))
    if line.upper()[0] in startChar:
        if line in d:
            d[line] = d[line] + 1 
        else:
            d[line] = 1



trigramfile.close()

outFile = open(basePath + "quotes.csv", "w")

for line in d:
    if d[line] > 1:
        outFile.write(line + "\t" + str(d[line]) + "\n")

outFile.close()
