import pandas as pd



infile = open("/data/wp-2022/output-b/sentiment.csv", "r")
lines = infile.readlines()
infile.close()

print("Number of articles: ", len(lines))

articles = []

for line in lines:
    rec = line.strip().split("\t")
    
    artTitle = rec[0]
    artValue = float(rec[1])
    posNumber = int(rec[2])
    negNumber = int(rec[3])
    tokNumber = int(rec[4])

    articles[artTitle] = artValue


articles = dict(sorted(articles.items(), key=lambda item: item[1]))






print("Done.")