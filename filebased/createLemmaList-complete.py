import lateco.Items as items
import collections

class Record:

    def __init__(self, lemma, tokens, pos):
        self.lemma = lemma
        self.tokens = tokens
        self.pos = pos




if __name__ == "__main__":
    lemmata = {}

    c = items.Corpus("/data/wp-2022/annotated/")
    c.setName("wp-2022")

    folders = c.getFolderList()    
    print("Found folders: ", len(folders))

    counter = 0
    for f in folders:
        for subcorpus in c.getSubcorporaOfFolder(f):
            print(counter, subcorpus)
            counter = counter + 1
            su = items.Subcorpus(subcorpus)
            for a in su.articles:
                for s in a.sentences:
                    for t in s.tokens:
                        if t.lemma in lemmata:
                            lemmata[t.lemma].tokens[t.token] += 1
                            lemmata[t.lemma].pos[t.pos] += 1
                        else:
                            tokList = collections.Counter()
                            tokList[t.token] += 1

                            posList = collections.Counter()
                            posList[t.pos] += 1

                            l = Record(t.lemma, tokList, posList)
                            lemmata[t.lemma] = l

    outfile = open("/data/lemmaPosTok.json", "w")

    counter = 1

    for l in lemmata:
        outfile.write('{ "index" : {"_id" : "' + str(counter) + '" } }\n')
        outfile.write('{"lemma" : "' + l.replace('"', "QUOTE").replace("{", "").replace("}", "").replace('\\', "") + '", "pos" : "'  )

        temp = ""
        for pos in lemmata[l].pos:
            temp =  temp + pos + ":" + str(lemmata[l].pos[pos]) + ";"
        temp = temp.strip(", ")
        outfile.write(temp)
        outfile.write("\", ")

        outfile.write(' "tokens" : "')
        temp = ""
        for tok in lemmata[l].tokens:
            temp =  temp + tok.replace("\"", "QUOTE").replace("{", "").replace("}", "").replace('\\', "") + ":" + str(lemmata[l].tokens[tok]) + ";"
        temp = temp.strip(";")
        outfile.write(temp)



        outfile.write("\"}\n")
        
        
        counter = counter + 1


    outfile.close()
