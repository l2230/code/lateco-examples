from wordcloud import WordCloud
import matplotlib.pyplot as plt
import sys
sys.path.insert(0,'..')

import settings 

wcFile = settings.config["outputfolder"] + "Concordance_wordcloud_" + sys.argv[1] + ".con"
lemmaFile = settings.config["outputfolder"] + "Concordance_lemma_" + sys.argv[1] + ".con"
sentencesFile = settings.config["outputfolder"] + "Concordance_sentences_" + sys.argv[1] + ".con"

print(wcFile)
print(lemmaFile)
print(sentencesFile)

with open(wcFile) as f:
    text = f.read()
print(text)
wordcloud = WordCloud(background_color="white", width=1920, height=1080, collocations=False).generate(text)

plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.savefig(settings.config["outputfolder"] + "Concordance_wordcloud_" + sys.argv[1] + ".png")
