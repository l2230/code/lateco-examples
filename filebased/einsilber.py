import collections
import re

prefixe = collections.Counter()
suffixe = collections.Counter()

pattern = re.compile(r'[^a-zA-Z0-9äöüÄÖÜß]+', re.UNICODE)

vokale = ["a", "e", "i", "o", "u", "ü", "ä", "ö", "y"]


def analyse(s):
    s = s.lower()
    result = False
    blocks = 0
    insideBlock = False
    for i in range(0, len(s)):
        if s[i] in vokale:
            if insideBlock == False:
                insideBlock == True
                blocks += 1
        else:
            insideBlock = False 
    
    if blocks == 1:
        return True
    else:
        return False

def fixe(s):
    s = s.lower()
    position = "l"
    prefix = ""
    suffix = ""

    for i in range(0, len(s)):
        if s[i] in vokale:
            position = "r"
        else:
            if position == "l":
                prefix = prefix + s[i]
            else:
                suffix = suffix + s[i]
    if not suffix.isnumeric():
        suffixe[suffix] += 1
    if not prefix.isnumeric():
        prefixe[prefix] += 1



if __name__ == "__main__":

    outfile = open("/data/wp-2022/listen/einsilber.csv", "w")

    with open("/data/wp-2022/listen/lemmataWithPOS.csv", "r") as infile:
        for line in infile:
            line = line.strip()
            rec = line.split("\t")
            #print(rec)
            #print(pattern.search(rec[0]))
            if (rec[1] != "NE") and (pattern.search(rec[0]) == None):
                if analyse(rec[0]):
                    outfile.write(rec[0] + "\n")
                    fixe(rec[0])
            #print("----------------")

    outfile.close()

    outfile = open("/data/wp-2022/listen/einsilberPrefixe.csv", "w")
    for entry in prefixe.most_common():
        outfile.write(entry[0] + "\t" + str(entry[1]) + "\n")
    outfile.close()

    outfile = open("/data/wp-2022/listen/einsilberSuffixe.csv", "w")
    for entry in suffixe.most_common():
        outfile.write(entry[0] + "\t" + str(entry[1]) + "\n")
    outfile.close()