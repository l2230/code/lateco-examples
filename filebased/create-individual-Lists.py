import lateco.Items as items
import collections
import sys
sys.path.insert(0,'..')

import settings

print("Now creating some lists ...")

# Global dictionaries to hold the aggregated lists
lemma = collections.Counter()
articles = {}
pos = collections.Counter()
typeToken = {}

# Function to iterate over a subcorpus and enter the found elements
def aggregate(subcorpus, position):
    su = items.Subcorpus(subcorpus)
    counter = 0
    for a in su.articles:
        position.article = a.id
        counter = counter + 1
        articles[a.title] = position.getArticle()       
     
        for s in a.sentences:
            for t in s.tokens:        
                lemma[t.lemma + "\t" + t.pos] += 1
                pos[t.pos] += 1      

                if t.lemma in typeToken:
                    if t.token in typeToken[t.lemma]:
                        typeToken[t.lemma][t.token] = typeToken[t.lemma][t.token] + 1
                    else:
                        typeToken[t.lemma][t.token] = 1
                else:
                    temp = {}
                    temp[t.token] = 1
                    typeToken[t.lemma] = temp

######################################################

if __name__ == "__main__" :
    # Getting the corpus
    c = items.Corpus(settings.config["corpuspath"])
    c.setName(sys.argv[1])
    position = items.Position()

    position.corpus = c.getName()    
    folders = c.getFolderList()    
    print("Found folders: ", len(folders))

    # Iterating over the subcorpora
    counter = 1
    for f in folders:
        position.folder = f.rstrip("/")
        for subcorpus in c.getSubcorporaOfFolder(f):
            position.subcorpus = subcorpus.split("/")[-1]
            aggregate(subcorpus, position)
            print(counter, subcorpus)
            counter = counter + 1

    # Writing the result to the outfile
    outfile = open(settings.config["outputfolder"] + "/lemmataWithPos.csv", "w")
    for w in sorted(lemma, key=lemma.get, reverse=True):
        outfile.write(w + "\t" +  str(lemma[w]) + "\n")        
    outfile.close()

    outfile = open(settings.config["outputfolder"] + "/articles.csv", "w")
    for a in articles:
        outfile.write(a + "\t" +  str(articles[a]) + "\n")        
    outfile.close()

    outfile = open(settings.config["outputfolder"] + "/pos.csv", "w")
    for p in sorted(pos, key=pos.get, reverse=True):
        outfile.write(p + "\t" +  str(pos[p]) + "\n")        
    outfile.close()

    outfile = open(settings.config["outputfolder"] + "/typeToken.csv", "w")
    for ty in typeToken:
        temp = ty + "\t"
        for tok in typeToken[ty]:
            temp = temp + tok + ";" + str(typeToken[ty][tok]) + "\t"
        
        outfile.write(temp + "\n")        
    outfile.close()
