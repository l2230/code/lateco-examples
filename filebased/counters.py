import lateco.Items as items

c = items.Corpus("/data/wp-2022/annotated/")

subcorpora = c.getSubcorpora()

tokenCount = 0
sentenceCount = 0
articleCount = 0

for subcorpus in subcorpora:
    su = items.Subcorpus(subcorpus)
    for article in su.articles:
        articleCount = articleCount + 1
        for sentence in article.sentences:
            sentenceCount = sentenceCount + 1
            tokenCount = tokenCount + len(sentence.tokens)

print("Number of Tokens: ", tokenCount)
print("Number of Sentences: ", sentenceCount)
print("Number of Articels: ", articleCount)

